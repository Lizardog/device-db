<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "device_table".
 *
 * @property int $id
 * @property string $vendor
 * @property string $model
 * @property int $asupim
 * @property int $device_check
 * @property int $idprint
 * @property int $copy_safe
 * @property string $driver
 * @property int $device_added
 */
class Device extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'device_added',
                ],
                'value' => function () {
                    return date('U');
                }
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device_table';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendor', 'model', 'asupim', 'device_check', 'idprint', 'copy_safe', 'driver'], 'required'],
            [['device_added'], 'integer'],
            [['asupim', 'device_check', 'idprint', 'copy_safe', ], 'boolean'],
            [['vendor', 'model', 'driver'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendor' => 'Вендор',
            'model' => 'Модель',
            'asupim' => 'АСУПИМ',// асупим статистика и асупим образы
            'device_check' => 'DeviceCheck', //devicecheck локальный порт, сетевой порт
            'idprint' => 'ID Print',
            'copy_safe' => 'CopySafe',
            'driver' => 'Driver',
            'device_added' => 'Дата добавления',
        ];
    }

    /**
     * {@inheritdoc}
     * @return DeviceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeviceQuery(get_called_class());
    }
}
