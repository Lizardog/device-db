<?php

use yii\helpers\Html;
use yii\grid\GridView;

const DATETIME_FORMAT = 'php:Y-m-d H:i:s';
/* @var $this yii\web\View */
/* @var $searchModel app\modelsSearch\DeviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Devices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Device', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'vendor',
            'model',
            [
                'attribute'=>'asupim',

                'label'=>'АСУПиМ',
                'content'=>function($data){
                    if($data->asupim) return '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
                    return '<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>';
                }
            ],
            [
                'attribute'=>'device_check',
                'label'=>'DeviceCheck',
                'content'=>function($data){
                    if($data->device_check) return '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
                    return '<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>';
                }
            ],
            [
                'attribute'=>'idprint',
                'label'=>'IDPrint',
                'content'=>function($data){
                    if($data->idprint) return '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
                    return '<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>';
                }
            ],
            [
                'attribute'=>'copy_safe',
                'label'=>'CopySafe',
                'content'=>function($data){
                    if($data->copy_safe) return '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
                    return '<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>';
                }
            ],
            [
                'attribute'=>'driver',
                'label'=>'Драйвер',
                'content'=>function($data){
                    return '<a href="http://google.com/'.$data->driver.'">Скачать</a>';
                }
            ],
            [
                'attribute'=>'device_added',
                'label'=>'Дата добавления',
                'content'=>function($data){
                    return Yii::$app->formatter->asDate($data->device_added, DATETIME_FORMAT);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
