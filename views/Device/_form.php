<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Device */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-6">
        <div class="form-inline">
            <?= $form->field($model, 'asupim')->checkbox() ?>
            <div class="form-group">&nbsp;</div>

            <?= $form->field($model, 'device_check')->checkbox() ?>

            <div class="form-group">&nbsp;</div>
            <?= $form->field($model, 'idprint')->checkbox() ?>
            <div class="form-group">&nbsp;</div>

            <?= $form->field($model, 'copy_safe')->checkbox() ?>
            <div class="form-group">&nbsp;</div>
        </div>

    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'vendor')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'driver')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'device_added')->textInput(['disabled' => true]) ?>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
