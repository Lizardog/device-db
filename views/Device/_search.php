<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modelsSearch\DeviceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vendor') ?>

    <?= $form->field($model, 'model') ?>

    <?= $form->field($model, 'asupim') ?>

    <?= $form->field($model, 'device_check') ?>

    <?php // echo $form->field($model, 'idprint') ?>

    <?php // echo $form->field($model, 'copy_safe') ?>

    <?php // echo $form->field($model, 'driver') ?>

    <?php // echo $form->field($model, 'device_added') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
